import axios from "axios";

let baseURL = process.env.NEXT_PUBLIC_BASE_URL;

export let apiInstance = axios.create({
  headers: {
    Accept: "application/json",
    Authorization: "Bearer {token}",
  }
});

// export const setAuthToken = (token: string) => {
//  if (token) {
//  //applying token
//  apiInstance.defaults.headers.common['Authorization'] = token;
//  } else {
//  //deleting the token from header
//  delete apiInstance.defaults.headers.common['Authorization'];
//  }
// }

apiInstance.interceptors.request.use(
  function(config) {
    const token = localStorage.getItem("token"); 
    if (token) {
      config.headers["Authorization"] = 'Bearer ' + token;
    }else {
      delete config.headers["Authorization"];
    }
    return config;
  },
  function(error) {
    return Promise.reject(error);
  }
);

console.log("baseURL:", baseURL);
