import React from "react";
import { Button, Box } from "@chakra-ui/react";
import { apiInstance } from "../api/baseUrl";
import { Wrapper } from "../components/Wrapper";
import { Container } from "@material-ui/core";
import { useRouter } from "next/router";

interface registerTokenProps {}

const RegisterToken: React.FC<registerTokenProps> = ({}) => {
  const router = useRouter();
  return (
    <Container>
      <Wrapper>
        <Box mt={6}> ایجاد یورز توکن </Box>
        <Button
          type={"submit"}
          colorScheme="purple"
          onClick={async () => {
            await apiInstance({
              method: "POST",
              url: "https://testshop.linkeee.ir/v1/StoreRegisterToken",
            })
              .then((response) => {
                console.log(response);
                console.log(response.data.data);
                if (response) {
                  localStorage.setItem("pageIndex", response.data.pageIndex);
                  localStorage.setItem("pageSize", response.data.pageSize);
                }
                router.push("/showToken");
              })
              .catch((err) => {
                // const error = err;
                //   console.log(err.data);
                // setErrors(error);
              });
          }}
        >
          ارسال
        </Button>
      </Wrapper>
    </Container>
  );
};

export default RegisterToken;
