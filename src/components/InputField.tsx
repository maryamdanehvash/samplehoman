import { useField } from "formik";
import React, { InputHTMLAttributes } from "react";
import TextField, { TextFieldProps } from "@material-ui/core/TextField";
import TextArea, { TextareaAutosizeProps } from "@material-ui/core/TextareaAutosize"

type InputFieldProps = InputHTMLAttributes<
  HTMLInputElement | TextFieldProps | TextareaAutosizeProps
> & {
  name: string;
  label: string;
  textarea?: boolean;
  text?: boolean;
  error?: any;
  helperText?: any;
  TextField?: TextFieldProps | TextareaAutosizeProps;
};

// '' => false, 'error message' => true
export const InputField: React.FC<InputFieldProps> = ({
  label,
  textarea,
  text,
  error,
  helperText,
  size: _,
  ...props
}) => {
  let TextFieldOrTextarea : any = TextField ;
  if (textarea) {
    TextFieldOrTextarea = TextArea;
  }

  const [ field ] = useField(props);

  return (
    <TextFieldOrTextarea
      variant="outlined"
      fullWidth
      id={field.name}
      label={label}
      {...props}
      {...field}
      error={error}
      helperText = {helperText}
    />
  );
};
