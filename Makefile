up:
	docker-compose up -d

down:
	docker-compose down

shell:
	docker-compose exec frontend sh

build:
	docker-compose build

buildup:
	docker-compose up --build -d