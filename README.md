# Making the project online

> Note: We are going to use Docker. As far as Docker restricted Iranian users, please use something like VPN or [Shecan](https://shecan.ir) to proceed the steps.

After installing docker, you should log in to our docker registry.

```
$ docker login registry.dinadev.ir
```

It will prompt you to login, use your gitlab username/password to authenticate.

After log in, use below command to build up your docker containers:

```
$ make up
```